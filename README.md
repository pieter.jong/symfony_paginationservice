# Symfony Service which makes use of the PagerFanta bundle

* <a target="_blank" href="https://github.com/whiteoctober/WhiteOctoberPagerfantaBundle">PagerFanta github</a>

When using > Symfony 4.0 you only need to composer install.

* php composer.phar require white-october/pagerfanta-bundle



## Example setup for a controller
* Copy `PagerfantaPaginationService.php` into you service folder. default this would be `src/Service`. Make sure autoload for this service is turned off. 


### Using the service
```php
<?php
private function getAllPaginatedCards(Request $request)
    {
        $searchString = $request->get('q');
        $queryBuilder = $this->em->getRepository(Cards::class)->findOrSearchCards($searchString);

        $page = $request->query->get('page', 1);
        $adapter = new DoctrineORMAdapter($queryBuilder);
        $pagerfanta = new Pagerfanta($adapter);
        $pagerfanta->setMaxPerPage(25);
        $pagerfanta->setCurrentPage($page);

        $cardData = [];
        /** @var Cards $card */
        foreach ($pagerfanta->getCurrentPageResults() as $card) {
            $cardData[] = $card;
        }
        // Here is ajax_list_datatable_all a route which wil be called when the paginationButtons are clicked
        $this->pagerfantaPagination->initialise($cardData, $pagerfanta, $page, 'ajax_list_datatable_all', $searchString); 

        return $this->pagerfantaPagination;
    }
```

### Example for initial loading a page

```php
<?php
public function overview(Request $request): Response
    {
        $cardData = $this->getAllPaginatedCards($request);

        return $this->render(
            '@App/overview_cards.html.twig', [
                'cards'    => $cardData,
                'searchString' => ''
            ]
        );
    }
    
```

### Example for pagination or search with, for example ajax
```php
<?php
public function getPaginatedCards(Request $request)
    {
        $cardData = $this->getAllPaginatedCards($request);
        $searchString = $request->get('q');
        return $this->render(
            '@App/listOverviewCards.html.twig', [
                'cards'    => $cardData,
                'searchString' => $searchString
            ]
        );
    }
```


## A template usage example

```twig
<div class="col-lg-3">
    <div class="form-group">
        <label class="control-label" for="notification_title">Zoeken</label>
        <input value="{% if searchString is defined %}{{ searchString }}{% endif %}" type="text"  class="form-control jq-on-search-confirm-ajax" ajax-url="{{ path('financial_invoice_ajax_invoices_send',{q:''}) }}" target-class=".ajax-container-send-invoices">

    </div>
</div>

<table class="table">
    <thead>
    <tr>
        <th scope="col">Factuur nr</th>
        <th scope="col">Relatie</th>
        <th scope="col">Totaal</th>
        <th scope="col">Datum</th>
        <th scope="col">Datum aangepast</th>
        <th scope="col">PDF: <input type="checkbox" title="Alles selecteren"
                                    onclick="checkAllInputsInCurrentTable(this, 'invoice_export_pdf')"/></th>
    </tr>
    </thead>
    <tbody>
    {% for invoice in sendInvoicesPaginated.data %}
        <tr class="{{ cycle(['bg-light', ''], loop.index0) }}">
            <th scope="row"><a href="{{ path('invoice_show',{id:invoice.id}) }}">OFF#{{ invoice.id }}TF#{{ invoice.id }}</a></th>
            <td>{{ invoice.1 }}</a></td>
            <td>€ {{ invoice.2 }}</td>
            <td>{{ invoice.3|date('d-m-Y H:i:s') }}</td>
            {% if invoice.4 %}
                <td>{{ invoice.4|date('d-m-Y H:i:s') }}</td>
            {% else %}
                <td class="text-center">-</td>
            {% endif %}
            <td><input type="checkbox" title="Exporteer pdf bestanden" class="invoice_export_pdf" name="invoice_export_pdf[]" value="{{ invoice.id }}"></td>
        </tr>
    {% endfor %}
    </tbody>
</table>

{% include '@App/GeneralBuildingBlocks/paginationButtons.html.twig' with {'paginatedObjects':sendInvoicesPaginated, 'targetClass':'.ajax-container-send-invoices'} %}
```

### paginationButtons.html.twig
Can be found in this repository

### javascript you could use for your templates

```javascript
<script>
    $(document).ready(function(){

        $(document).on('click', '.jq-on-click-ajax', function(object){
            let ajax_url= this.getAttribute('ajax-url');
            let target_class = this.getAttribute('target-class');
            let dropdown_value = $(this).val();
            ajax(ajax_url, target_class)
        });

        $(document).on('keypress', '.jq-on-search-confirm-ajax', function(object) {
            if(object.keyCode == 13)
            {
                object.preventDefault();
                let ajax_url= this.getAttribute('ajax-url');
                let target_class = this.getAttribute('target-class');
                let search_value = $(this).val();
                ajax_url = ajax_url + search_value
                ajax(ajax_url, target_class)
            }
        });

    });

    function ajax(ajax_url, target_class) {
        $.ajax({
            url : ajax_url,
            method : 'get',

            success : function(response){
                $(target_class).html(response);
            }
        });
    }
</script>
```



