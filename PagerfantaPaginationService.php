<?php


namespace AppBundle\Service\GeneralServices;

use Pagerfanta\Pagerfanta;
use Symfony\Component\Routing\RouterInterface;

class PagerfantaPaginationService
{

    private $data;

    private $pageCount;

    private $totalRecords;

    private $firstRecordOfPage;

    private $lastRecordOfPage;

    private $_links = array();

    private $draw;

    /**
     * @var int $currentPage
     */
    private $currentPage;

    /**
     * @var string $route
     */
    private $route;

    /**
     * @var array $routeParams
     */
    private $routeParams = [];

    /**
     * @var RouterInterface
     */
    private $routerInterface;

    /**
     * @var Pagerfanta $pagerfanta
     */
    private $pagerfanta;

    /**
     * @var string|null
     */
    private $searchString;

    public function __construct(RouterInterface $routerInterface)
    {
        $this->routerInterface = $routerInterface;
    }

    private function addDefaultLinks(): void
    {
        $this->addLink('current', $this->createLinkUrl($this->currentPage));
        $this->addLink('first', $this->createLinkUrl(1));

        $this->addLink('last', $this->createLinkUrl($this->pagerfanta->getNbPages()));

        if ($this->pagerfanta->hasNextPage()) {
            $this->addLink('next', $this->createLinkUrl($this->pagerfanta->getNextPage()));
        }
        if ($this->pagerfanta->hasPreviousPage()) {
            $this->addLink('prev', $this->createLinkUrl($this->pagerfanta->getPreviousPage()));
        }

        $this->addDynamicPages();
    }

    /**
     * @param $ref
     * @param $url
     */
    private function addLink($ref, $url): void
    {
        $this->_links[$ref] = $url;
    }

    /**
     * @param Pagerfanta $pagerfanta
     */
    private function setPagerfanta(Pagerfanta $pagerfanta): void
    {
        $this->pagerfanta = $pagerfanta;
    }

    /**
     * @param $url
     * @param $title
     */
    private function addDynamicPageLink($url, $title): void
    {
        $this->_links['dynamicPageLinks'][] = [
            'url'   => $url,
            'title' => $title
        ];
    }

    /**
     * @param int $maxPages
     */
    private function addDynamicPages($maxPages = 6): void
    {
        $prePages = intval(floor($maxPages / 2));
        if ($this->currentPage - $prePages + 1 < 1) {
            $prePages = 0;
        } else {
            if ($this->currentPage - $prePages + 1 <= $prePages) {
                $prePages = $this->currentPage - $prePages + 1;
            }
        }

        for ($i = $this->currentPage - $prePages; $i <= $this->currentPage + ($maxPages - $prePages); $i++) {
            $title = $i;
            if ($i !== 1 && $i < $this->pageCount) {
                $url = $this->createLinkUrl($title);
                $this->addDynamicPageLink($url, $title);
            }
        }
    }

    /**
     * @param $targetPage
     * @return string
     */
    private function createLinkUrl($targetPage): string
    {
        return $this->routerInterface->generate($this->route, array_merge(
            $this->routeParams,
            array('page' => $targetPage,'q'=>$this->searchString)
        ));
    }

    private function setDefaultCountValues(): void
    {
        $this->totalRecords = $this->pagerfanta->getNbResults();
        $this->pageCount = ceil($this->totalRecords / $this->pagerfanta->getMaxPerPage());
        $this->firstRecordOfPage = ($this->currentPage - 1) * $this->pagerfanta->getMaxPerPage() + 1;
        $this->lastRecordOfPage = ($this->currentPage - 1) * $this->pagerfanta->getMaxPerPage() + count($this->data);
    }

    /**
     * @return int
     */
    public function getCurrentPage(): int
    {
        return $this->currentPage;
    }

    /**
     * @return array
     */
    public function getData(): array
    {
        return $this->data;
    }

    /**
     * @param array $data
     * @return PagerfantaPaginationService
     */
    private function setData(array $data): PagerfantaPaginationService
    {
        $this->data = $data;
        return $this;
    }

    /**
     * @return float|int
     */
    public function getPageCount()
    {
        return $this->pageCount;
    }

    /**
     * @return mixed
     */
    public function getTotalRecords()
    {
        return $this->totalRecords;
    }

    /**
     * @return mixed
     */
    public function getFirstRecordOfPage()
    {
        return $this->firstRecordOfPage;
    }

    /**
     * @return mixed
     */
    public function getLastRecordOfPage()
    {
        return $this->lastRecordOfPage;
    }


    /**
     * @return array
     */
    public function getLinks(): array
    {
        return $this->_links;
    }

    /**
     * @return int
     */
    public function getDraw(): int
    {
        return $this->draw;
    }

    /**
     * @param int $currentPage
     * @return PagerfantaPaginationService
     */
    private function setCurrentPage(int $currentPage): PagerfantaPaginationService
    {
        $this->currentPage = $currentPage;
        return $this;
    }

    /**
     * @param string $route
     */
    private function setRoute(string $route): void
    {
        $this->route = $route;

        $this->setDefaultCountValues();
        $this->addDefaultLinks();
    }

    /**
     * @return array
     */
    public function getRouteParams(): array
    {
        return $this->routeParams;
    }

    /**
     * @param array $routeParams
     * @return PagerfantaPaginationService
     */
    public function setRouteParams(array $routeParams): PagerfantaPaginationService
    {
        $this->routeParams = $routeParams;
        return $this;
    }

    /**
     * @param array $invoiceData
     * @param Pagerfanta $pagerfanta
     * @param int $page
     * @param string $routeName
     */
    public function initialise(array $invoiceData, Pagerfanta $pagerfanta, int $page, string $routeName, ?string $searchString): void
    {
        $this->searchString = $searchString;
        $this->setData($invoiceData);
        $this->setPagerfanta($pagerfanta);
        $this->setCurrentPage($page);
        $this->setRoute($routeName);
    }

}
